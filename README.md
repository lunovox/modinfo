![screenshot]

![favicon]

# MODINFO
[Minetest MOD] 

Displays the list of mods used on the server, and the 'README.md' of each mod. Allows you to exclude some sensitive security mods from the list.

**Licence:**

* GNU AGPL: https://gitlab.com/lunovox/modinfo/-/raw/master/LICENSE

More details: 
 * English: https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License
 * Portuguese: https://pt.wikipedia.org/wiki/GNU_Affero_General_Public_License

**Depends:**

* --== NOTHING ==--

**Repository:**

* https://gitlab.com/lunovox/modinfo

**Commands:**

* ````/modinfo```` or ````/mods```` or ````/m```` : Displays the public list of installed mods.

**Settings:**

You don't need to do any of these presets mentioned below to make this mod work. But it's worth knowing the configuration values ​​in case you want to directly change the ````minetest.conf```` file.

* ````modinfo.secret_mods = <string>```` : Enter the comma-separated list of mods that will not be listed by modinfo.

**Developers:**

* Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [xmpp](xmpp:lunovox@disroot.org?join), [social web](http:mastodon.social/@lunovox), [audio conference](mumble:libreplanetbr.org), [more contacts](https:libreplanet.org/wiki/User:Lunovox)

[favicon]:https://gitlab.com/lunovox/modinfo/-/raw/master/favicon.png
[Minetest MOD]:https://www.minetest.net/
[screenshot]:https://gitlab.com/lunovox/modinfo/-/raw/master/screenshot.png

