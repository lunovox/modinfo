--[[
function string:split(pat)
  pat = pat or '%s+'
  local st, g = 1, self:gmatch("()("..pat..")")
  local function getter(segs, seps, sep, cap1, ...)
    st = sep and seps + #sep
    return self:sub(segs, (seps or 0) - 1), cap1 or sep, ...
  end
  return function() if st then return getter(st, g()) end end
end
--]]

modModinfo = {
	listReal = minetest.get_modnames(),
	listPublic = {},
	listDescription = {},
}

modModinfo.getListSecret = function()
	local conf = minetest.settings:get("modinfo.secret_mods")
	local confTable = {}
	if conf ~= nil and type(conf)=="string" and conf ~= "" then
		confTable = conf:trim():split(",")
	end
	return confTable
end

table.sort(modModinfo.listReal)
for i, modname in ipairs(modModinfo.listReal) do
	local isForbidden = false
	for i, modForbidden in ipairs(modModinfo.getListSecret()) do
		if modname == modForbidden:trim() then
			isForbidden = true
		end
	end
	if not isForbidden then
		table.insert(modModinfo.listPublic, modname)
		
		local handler = io.open(minetest.get_modpath(modname).."/README.md" , "rb")
		if handler then
			local content = handler:read("*all")
			handler:close()
			--content = string.gsub(content, "\r?\n", "\n")
			--content = string.gsub(content, "\r", "\n")
			--content = string.gsub(content, "\n", "")
			modModinfo.listDescription[modname] = content
		else
			handler = io.open(minetest.get_modpath(modname).."/mod.conf" , "rb")
			if handler then
				local content = handler:read("*all")
				handler:close()
				if modModinfo.listDescription[modname] == nil then
					modModinfo.listDescription[modname] = content
				else
					modModinfo.listDescription[modname] = modModinfo.listDescription[modname] .. "\n\n"..content
				end
			end
		end
	end
end


modModinfo.showFormSpec = function(playername, selModIndex)
	local formspecmods = ""
	local selModName = ""
	local selModDescription = ""
	
	formspecmods = table.concat(modModinfo.listPublic,",")
	
	--print("modModinfo.listReal = "..dump(modModinfo.listReal))
	--print("modModinfo.listPublic = "..dump(modModinfo.listPublic))
	--print("formspecmods = "..formspecmods)
	--[[print(
		"type(selModIndex) = "..type(selModIndex)
		.." | selModIndex = "..dump(selModIndex)
		.." | #modModinfo.listPublic = "..#modModinfo.listPublic
		--.." | modModinfo.listPublic["..selModIndex.."] = "..dump(modModinfo.listPublic[selModIndex])
	)--]]
	if selModIndex == nil or type(selModIndex)~="number" or (selModIndex < 0 and selModIndex > #modModinfo.listPublic) then
		--print("aaaaaaaaaaaaaa")
		selModIndex = 0
	else
		--print("bbbbbbbbbbbbbbbb")
		selModName = modModinfo.listPublic[selModIndex]
		minetest.log('action', ("The player '%s' is examining mod %s."):format(playername, dump(selModName)))
		if 
			modModinfo.listDescription[selModName] ~= nil 
			and type(modModinfo.listDescription[selModName]) == "string" 
			and modModinfo.listDescription[selModName] ~= "" 
		then
			selModDescription = minetest.formspec_escape(modModinfo.listDescription[selModName])
		end
	end
	--print("selModIndex = "..selModIndex.." | selModName = "..dump(selModName))
	local formspec = "size[14,7.5]" --Default: w=8, h=7.5
	.. default.gui_bg
	.. default.gui_bg_img
	--.."bgcolor[#080808BB;true]"
	
	.."background[5,5;1,1;gui_formbg.png;true]"
	--.."label[2.75,0;MENSAGEM DE: "..mail.namefrom.."]"

	.."label[0,0;"..minetest.formspec_escape("MODS:").."]"	
	.."textlist[0,0.45;3,6;selmod;"..formspecmods..";"..selModIndex	..";false]"
	
	.."label[3.45,0;"..minetest.formspec_escape("DESCRIPTION:").."]"	
	.."box[3.25,0.45;10.5,6;#111111]"
	.."textarea[3.55,0.45;10.7,7;;;"..selModDescription.."]"

	.."button_exit[6,7;2,0.5;closer;"..minetest.formspec_escape("CLOSE").."]"

	minetest.show_formspec(playername,"modinfo.showFormSpec",formspec)
end


local propCommMods = function()
	return { 
		params="", 
		privs={},
		description = "Displays the public list of installed mods.",
		func = function(playername, param)
			--[[
			for i, modname in ipairs(modModinfo.listPublic) do
				modModinfo.listPublic
			end	
			--]]
			modModinfo.showFormSpec(playername)
			return true
		end,
	}
end

minetest.register_chatcommand("modinfo", propCommMods())
minetest.register_chatcommand("mods", propCommMods())
minetest.register_chatcommand("m", propCommMods())


minetest.register_on_player_receive_fields(function(sender, formname, fields)
	local sendername = sender:get_player_name()
	--print(sendername.." >>> ["..formname.."] = "..dump(fields))
	if formname == "modinfo.showFormSpec"  then
		if fields.quit == nil and fields.closer == nil then
			local selnum = 0
			if fields.selmod ~= nil then
				selnum = fields.selmod:gsub("CHG:", "")
			end
			--print("modModinfo.showFormSpec('"..sendername.."', "..selnum..")")
			modModinfo.showFormSpec(sendername, tonumber(selnum))
		end
	end
end)

--print(dump(modModinfo.listDescription["modinfo"]))
